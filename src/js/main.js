var game = new Game()	//实例化游戏对象
game.init()		//调用方法
game.addSuccessFn(success)		//成功 继续游戏
game.addFailedFn(failed)		//失败

var isplay = false;	//防止音乐重复播放的标志位
//querySelector() 方法返回匹配指定 CSS 选择器元素的第一个子元素
var mask = document.querySelector('.mask')
var restartButton = document.querySelector('.restart')
var score = document.querySelector('.score')

restartButton.addEventListener('click', restart)	//“重新开始”的鼠标事件监听，开始下一局

// 游戏重新开始，执行函数
function restart () {
	mask.style.display = 'none'		//元素隐藏，隐藏失败后的弹窗
	game.restart()
}
// 游戏失败执行函数
function failed(){
	score.innerText = game.score
	mask.style.display = 'flex'		//flex:弹性布局，用来为盒状模型提供最大的灵活性
}
// 游戏成功，更新分数
function success (score) {
	//querySelector() 方法返回匹配指定 CSS 选择器元素的第一个子元素  匹配失败后的弹窗中的得分页面
	var scoreCurrent = document.querySelector('.score-current')		
	scoreCurrent.innerText = score;
	// 记录最高分
	var record = document.querySelector('.record');
	var item = 'JUMP_KING_RECORD_SCORE';
	var itemScore = parseInt(localStorage.getItem(item) || 0);	//parseInt字符串转换为10进制数
	if( itemScore < score){
		//localStorage特性：永久性存储，这个特性主要是用来作为本地存储来使用的，相当于一个5M大小的针对于前端页面的数据库
		//sessionStorage：临时性存储
		localStorage.setItem(item, score);		//将score存储到item字段
		record.innerText = score;		//innerText：设置或获取标签所包含的文本信息（从标签起始位置到终止位置的内容，去除HTML标签，但不包括自身）
	}else{
		record.innerText = itemScore;
	}
}

// 背景音乐/音效
function audioBgm() {
	if(!isplay){
		var bgm = new Audio('./src/bgm.mp3');
		bgm.volume = .05
		bgm.play();
		isplay = true;
	}
}
//监听鼠标点击事件，第一次点击页面后播放背景音乐
document.getElementById('box_body').addEventListener('click', function(){
	audioBgm();
})

var ActMusic = new Audio('./src/jump.mp3');
ActMusic.volume = .05;
ActMusic.loop = false;

var FallMusic = new Audio('./src/fall.mp3');
FallMusic.volume = .05;
FallMusic.loop = false;
