var Game = function () {
  // 基本参数
  this.config = {
    background: 0xFFFFE0, // 背景颜色
    ground: -1, // 地面y坐标
    fallingSpeed: 0.2, // 游戏失败掉落速度
    cubeColor: 0xbebebe,//方块颜色
    cubeWidth: 3, // 方块宽度
    cubeHeight: 1, // 方块高度
    cubeDeep: 3, // 方块深度
    jumperColor: 0x7FFFD4,
    jumperWidth: 1, // jumper宽度
    jumperHeight: 2, // jumper高度
    jumperDeep: 1, // jumper深度
  }
  // 游戏状态
  this.score = 0  //初始化得分为0
  this.size = {
    //返回窗口的文档显示区的宽度和高度
    width: window.innerWidth,
    height: window.innerHeight
  }
  this.scene = new THREE.Scene()    //场景对象，相当于一个容器
  //three.scane讲解：https://www.hangge.com/blog/cache/detail_1783.html
  this.cameraPos = {
    current: new THREE.Vector3(0, 0, 0), // 摄像机当前的坐标  三维向量
    next: new THREE.Vector3() // 摄像机即将要移到的位置
  }
  //正交摄像机：正交摄像机投影物体是通过平面，无论距离有多远，投射到二维平面的线始终的是平行的，所以看上去就会感觉物体的大小没有受到任何影响
  /*
  new THREE.OrthographicCamera（）构造函数用于创建一个正交摄像机，该构造函数中有六个参数，分别是left，right，top，bottom，near，far。
    left — 摄像机视锥体左侧面。
    right — 摄像机视锥体右侧面。
    top — 摄像机视锥体上侧面。
    bottom — 摄像机视锥体下侧面。
    near — 摄像机视锥体近端面。
    far — 摄像机视锥体远端面。
  */
  this.camera = new THREE.OrthographicCamera(this.size.width / -80, this.size.width / 80, this.size.height / 80, this.size.height / -80, 0, 5000)
  this.renderer = new THREE.WebGLRenderer({antialias: true})  //设置为true开启反锯齿
  var planceGeometry = new THREE.PlaneGeometry(this.size.width, this.size.height);    // PlaneGeometry:平面几何    (参数: 宽60, 高20)
  // MeshLambertMaterial 用于暗淡不光亮表面的材质，color属性是漫反射的颜色，默认为白色。
  var planeMaterial = new THREE.MeshLambertMaterial({ color: 0xFFFAF0 });    // MeshLambertMaterial:网格材质    (用来设置平面的外观, 颜色，透明度等)
  var plane = new THREE.Mesh(planceGeometry, planeMaterial);    // 把这2个对象合并到一个名为plane(平面)的Mesh(网格)对象中
  plane.receiveShadow = true;    // 平面接收阴影
  plane.rotation.x = -0.5*Math.PI;    // 平面绕x轴旋转90度
  plane.position.x = 0;    // 平面坐标位置
  plane.position.y = -1;
  plane.position.z = 0;
  this.plane = plane;
  this.scene.add(this.plane);    // 将平面添加到场景

  this.cubes = [] // 方块数组
  this.cubeStat = {
    nextDir: '' // 下一个方块相对于当前方块的方向: 'left' 或 'right'
  }
   //跳起
  this.jumperStat = {  
    ready: false, // 鼠标按完没有
    xSpeed: 0, // xSpeed根据鼠标按的时间进行赋值
    ySpeed: 0  // ySpeed根据鼠标按的时间进行赋值
  }
  //落下
  this.falledStat = {
    location: -1, // jumper所在的位置
    distance: 0 // jumper和最近方块的距离
  }
  //正在落下
  this.fallingStat = {
    speed: 0.2, // 游戏失败后垂直方向上的掉落速度
    end: false // 掉到地面没有
  }
  this.combo = 0; // 连续掉落到中心的次数，起始为0
}
Game.prototype = {
  init: function () {
    // this._checkUserAgent() // 检测是否移动端
    this._setCamera() // 设置摄像机位置
    this._setRenderer() // 设置渲染器参数
    this._setLight() // 设置光照
    this._createCube() // 加一个方块
    this._createCube() // 再加一个方块
    this._createJumper() // 加入游戏者jumper
    this._updateCamera() // 更新相机坐标
    

    var self = this
    var mouseEvents=
    {
      down: 'mousedown',
      up: 'mouseup',
    }
     
   
// 事件监听绑定到canvas中
    //<canvas>（画布）是HTML 5 新增的元素，可用于通过使用JavaScript中的脚本来绘制图形
    var canvas = document.querySelector('canvas')
    canvas.addEventListener(mouseEvents.down, function () {
      self._handleMousedown()
    })
    // 监听鼠标松开的事件
    canvas.addEventListener(mouseEvents.up, function (evt) {
      self._handleMouseup()
    })
    // 监听窗口变化的事件
    window.addEventListener('resize', function () {
      self._handleWindowResize()
    })
  },

// 游戏失败重新开始的初始化配置
  restart: function () {
    this.score = 0  //得分
    this.cameraPos = {  //摄像机
      current: new THREE.Vector3(0, 0, 0),
      next: new THREE.Vector3()
    }
    this.fallingStat = {  //下落  游戏失败后垂直方向上的掉落速度
      speed: 0.2,
      end: false
    }
    // 删除所有方块
    var length = this.cubes.length
    for(var i=0; i < length; i++){
      this.scene.remove(this.cubes.pop())
    }
    // 删除jumper
    this.scene.remove(this.jumper)
    // 显示的分数设为 0
    this.successCallback(this.score)
    this._createCube()
    this._createCube()
    this._createJumper()
    this._updateCamera()
  },

/**
 * jQuery是一个快速、简洁的JavaScript框架，是继Prototype之后又一个优秀的JavaScript代码库（或JavaScript框架）。
 * jQuery设计的宗旨是“write Less，Do More”，即倡导写更少的代码，做更多的事情。
 * 它封装JavaScript常用的功能代码，提供一种简便的JavaScript设计模式，
 * 优化HTML文档操作、事件处理、动画设计和Ajax交互。
jQuery的核心特性可以总结为：
具有独特的链式语法和短小清晰的多功能接口；
具有高效灵活的css选择器，并且可对CSS选择器进行扩展；
拥有便捷的插件扩展机制和丰富的插件。
 */

// 游戏成功的执行函数, 外部传入
  addSuccessFn: function (fn) {
    this.successCallback = fn
  },
 // 游戏失败的执行函数, 外部传入
  addFailedFn: function (fn) {
    this.failedCallback = fn
  },
  
// 窗口缩放绑定的函数
  _handleWindowResize: function () {
    this._setSize()
    this.camera.left = this.size.width / -80
    this.camera.right = this.size.width / 80
    this.camera.top = this.size.height / 80
    this.camera.bottom = this.size.height / -80
    this.camera.updateProjectionMatrix()  //更新投影矩阵
    this.renderer.setSize(this.size.width, this.size.height)
    this._render()  //渲染方法
  },
  /**
   *鼠标按下绑定的函数
   *根据鼠标按下的时间来给 xSpeed 和 ySpeed 赋值
   *@return {Number} this.jumperStat.xSpeed 水平方向上的速度
   *@return {Number} this.jumperStat.ySpeed 垂直方向上的速度
  **/
  _handleMousedown: function () {
    var self = this
    function act() {
      // 以jumperBody蓄力一半为最大值
      if (!self.jumperStat.ready && self.jumperBody.scale.y > 0.02 &&  self.jumperBody.scale.y >= 0.5) {
        self.jumperBody.scale.y -= 0.01  // jumper随按压时间降低高度，即减小jumper.scale.y值
        self.jumperHead.position.y -= 0.02 // jumper头部跟随下降

        self.jumperStat.xSpeed += 0.004
        self.jumperStat.ySpeed += 0.008

        self.mouseDownFrameHandler =  requestAnimationFrame(act); //调用api接口，在浏览器下次重绘之前继续更新下一帧动画
      }
      self._render(self.scene, self.camera);  //渲染
    }
    act();
  },
  // 鼠标松开绑定的函数
  _handleMouseup: function () {
    var self = this
    // 标记鼠标已经松开
    self.jumperStat.ready = true;
    cancelAnimationFrame(self.mouseDownFrameHandler); //用于取消以前通过对window.requestAnimationFrame()的调用计划的动画帧请求
    var frameHandler;

    function act() {
      // 判断jumper是在方块水平面之上，是的话说明需要继续运动
      // 判断jumper的位置，并判断jumper的y速度
      if (self.jumperStat.ySpeed > 0 || self.jumper.position.y >= 1) {
        // jumper根据下一个方块的位置来确定水平运动方向

        if (self.cubeStat.nextDir === 'left') {   //下一个方块出现在左边，x轴变化
          self.jumper.position.x -= self.jumperStat.xSpeed
          
        } else {  //下一个方块出现在右边，z轴变化
          self.jumper.position.z -= self.jumperStat.xSpeed
         
        }

        // jumper在垂直方向上运动
        self.jumper.position.y += self.jumperStat.ySpeed
        // 运动伴随着缩放
        if ( self.jumperBody.scale.y < 1 ) {
          self.jumperBody.scale.y += 0.02;
          self.jumperHead.position.y += 0.02 // 头部跟随上升
        }
        // jumper在垂直方向上先上升后下降
        self.jumperStat.ySpeed -= 0.01
        // 每一次的变化，渲染器都要重新渲染，才能看到渲染效果
        self._render(self.scene, self.camera)

        frameHandler = requestAnimationFrame(act);  //api接口 继续更新下一帧动画
      }else{
        cancelAnimationFrame(frameHandler);
        landed();
      }
    }

    function landed() {

      // 用于测试combo，手动设置掉到正中心
      // self.jumper.position.x = self.cubes[self.cubes.length - 1].position.x;
      // self.jumper.position.z =  self.cubes[self.cubes.length - 1].position.z;

      // jumper掉落到方块水平位置，开始重置状态，并开始判断掉落是否成功
      self.jumperStat.ready = false
      self.jumperStat.xSpeed = 0
      self.jumperStat.ySpeed = 0
      self.jumper.position.y = .5
      // 还原jumper的旋转角度和head的位置
      self.jumper.rotation.z = 0
      self.jumper.rotation.x = 0
      self.jumperHead.position.y = 0

      self._checkInCube();

      if (self.falledStat.location === 1) {
        // 播放掉落成功音效
        if(ActMusic){
          ActMusic.play();
        }

        // 掉落成功，进入下一步
        self.score += Math.pow(1, self.combo); // 随着combo
        self._createCube()
        self._updateCamera()

        if (self.successCallback) {
          self.successCallback(self.score)
        }
      } else {
        // 掉落失败，进入失败动画
        self._falling();

        // // 播放掉落失败音效
        if(FallMusic){
          FallMusic.play();
        }
      }
    }

    act();
  },
  /**
   *游戏失败执行的碰撞效果
   *@param {String} dir 传入一个参数用于控制倒下的方向：'rightTop','rightBottom','leftTop','leftBottom','none'
  **/
  _fallingRotate: function (dir) {
    var self = this
    var offset = self.falledStat.distance - self.config.cubeWidth / 2
    var rotateAxis = 'z' // 旋转轴
    var rotateAdd = self.jumper.rotation[rotateAxis] + 0.1 // 旋转速度
    var rotateTo = self.jumper.rotation[rotateAxis] < Math.PI/2 // 旋转结束的弧度
    var fallingTo = self.config.ground + self.config.jumperWidth / 2 + offset

    if (dir === 'rightTop') {
      rotateAxis = 'x'
      rotateAdd = self.jumper.rotation[rotateAxis] - 0.1
      rotateTo = self.jumper.rotation[rotateAxis] > -Math.PI/2
      self.jumper.translate.z = offset
    
    } else if (dir === 'rightBottom') {
      rotateAxis = 'x'
      rotateAdd = self.jumper.rotation[rotateAxis] + 0.1
      rotateTo = self.jumper.rotation[rotateAxis] < Math.PI/2
      self.jumper.translate.z = -offset
    
    } else if (dir === 'leftBottom') {
      rotateAxis = 'z'
      rotateAdd = self.jumper.rotation[rotateAxis] - 0.1
      rotateTo = self.jumper.rotation[rotateAxis] > -Math.PI/2
      self.jumper.translate.x = -offset
     
    } else if (dir === 'leftTop') {
      rotateAxis = 'z'
      rotateAdd = self.jumper.rotation[rotateAxis] + 0.1
      rotateTo = self.jumper.rotation[rotateAxis] < Math.PI/2
      self.jumper.translate.x = offset
     
    } else if (dir === 'none') {
      rotateTo = false
      fallingTo = self.config.ground
    } else {
      throw Error('Arguments Error')
    }
    if (!self.fallingStat.end) {
      if (rotateTo) {
        self.jumper.rotation[rotateAxis] = rotateAdd
      } else if (self.jumper.position.y > fallingTo) {
        self.jumper.position.y -= self.config.fallingSpeed
      } else {
        self.fallingStat.end = true
      }
      self._render()
      requestAnimationFrame(function(){
        self._falling()
      })
    } else {
      if (self.failedCallback) {
        self.failedCallback()
      }
    }
  },
  /**
   *游戏失败进入掉落阶段
   *通过确定掉落的位置来确定掉落效果
  **/
  _falling: function () {
    var self = this
    if (self.falledStat.location == 0) {
      self._fallingRotate('none')
    } else if (self.falledStat.location === -10) {
      if (self.cubeStat.nextDir == 'left') {
        self._fallingRotate('leftTop')
      } else {
        self._fallingRotate('rightTop')
      }
    } else if (self.falledStat.location === 10) {
      if (self.cubeStat.nextDir == 'left') {
        if (self.jumper.position.x < self.cubes[self.cubes.length - 1].position.x) {
          self._fallingRotate('leftTop')
        } else {
          self._fallingRotate('leftBottom')
        }
      } else {
        if (self.jumper.position.z < self.cubes[self.cubes.length - 1].position.z) {
          self._fallingRotate('rightTop')
        } else {
          self._fallingRotate('rightBottom')
        }
      }
    }
  },
  /**
   *判断jumper的掉落位置
   *@return {Number} this.falledStat.location
   * -1 : 掉落在原来的方块，游戏继续
   * -10: 掉落在原来方块的边缘，游戏失败
   *  1 : 掉落在下一个方块，游戏成功，游戏继续
   *  10: 掉落在下一个方块的边缘，游戏失败
   *  0 : 掉落在空白区域，游戏失败
  **/
  _checkInCube: function () {

    if (this.cubes.length > 1) {
      var cubeScore = 0;
      // jumper 的位置
      var pointO = {
        x: this.jumper.position.x,
        z: this.jumper.position.z
      }
      // 当前方块的位置
      var pointA = {
        x: this.cubes[this.cubes.length - 1 - 1].position.x,
        z: this.cubes[this.cubes.length - 1 - 1].position.z
      }
      // 下一个方块的位置
      var pointB = {
        x: this.cubes[this.cubes.length - 1].position.x,
        z: this.cubes[this.cubes.length - 1].position.z
      }
      var distanceS, // jumper和当前方块的坐标轴距离
          distanceL;  // jumper和下一个方块的坐标轴距离
      // 判断下一个方块相对当前方块的方向来确定计算距离的坐标轴

      if (this.cubeStat.nextDir === 'left') {
        distanceS = Math.abs(pointO.x - pointA.x)
        distanceL = Math.abs(pointO.x - pointB.x)
      } else {  //下一个方块出现在右边
        distanceS = Math.abs(pointO.z - pointA.z)
        distanceL = Math.abs(pointO.z - pointB.z)
      }
      var should = this.config.cubeWidth / 2 + this.config.jumperWidth /2
      var result = 0
      if (distanceS < should ) {
        // 落在当前方块，将距离储存起来，并继续判断是否可以站稳
        this.falledStat.distance = distanceS
        //掉在原来的方块或者原方块的边缘
        result = distanceS < this.config.cubeWidth / 2 ? -1 : -10
      } else if (distanceL < should) {
        this.falledStat.distance = distanceL
        // 落在下一个方块，将距离储存起来，并继续判断是否可以站稳
        //掉在下一个的方块或者下一个方块的边缘
        result = distanceL < this.config.cubeWidth / 2 ? 1 : 10

        if(pointO.x == pointB.x && pointO.z == pointB.z){
          this.combo++;
        }else{
          this.combo = 0;
        }

      } else {
        result = 0
      }
      this.falledStat.location = result;
    }
  },

 // 每成功一步, 重新计算摄像机的位置，保证游戏始终在画布中间进行
  _updateCameraPos: function () {
    var lastIndex = this.cubes.length - 1
    var pointA = {
      x: this.cubes[lastIndex].position.x,
      z: this.cubes[lastIndex].position.z
    }
    var pointB = {
      x: this.cubes[lastIndex - 1].position.x,
      z: this.cubes[lastIndex - 1].position.z
    }
    var pointR = new THREE.Vector3()
    pointR.x = (pointA.x + pointB.x) / 2
    pointR.y = 0
    pointR.z = (pointA.z + pointB.z) / 2
    this.cameraPos.next = pointR
  },
  // 基于更新后的摄像机位置，重新设置摄像机坐标
  _updateCamera: function () {
      var self = this
      var c = {
        x: self.cameraPos.current.x,
        y: self.cameraPos.current.y,
        z: self.cameraPos.current.z
      }
      var n = {
        x: self.cameraPos.next.x,
        y: self.cameraPos.next.y,
        z: self.cameraPos.next.z
      }
      if (c.x > n.x  || c.z > n.z) {
        self.cameraPos.current.x -= 0.1
        self.cameraPos.current.z -= 0.1
        if (self.cameraPos.current.x - self.cameraPos.next.x < 0.05) {
          self.cameraPos.current.x = self.cameraPos.next.x
        }
        if (self.cameraPos.current.z - self.cameraPos.next.z < 0.05) {
          self.cameraPos.current.z = self.cameraPos.next.z
        }
        //相机lookAt的点一定显示在屏幕的正中央：利用这点，我们可以实现物体移动时，我们可以一直跟踪物体，让物体永远在屏幕的中央
        self.camera.lookAt(new THREE.Vector3(c.x, 0, c.z))  //相机观察的目标点
        // 更新光源
        self._render()
        requestAnimationFrame(function(){
          self._updateCamera()
        })
      }
  },
  // 初始化jumper：游戏主角
  _createJumper: function () {
    var self = this
    //three创建材质。这种材质可以用来创建暗淡的并不光亮的表面。
    var material = new THREE.MeshLambertMaterial({color: this.config.jumperColor})
    //简单三维几何体 圆柱形jumper 顶部半径 底部半径 高度
    var bodyGeometry = new THREE.CylinderGeometry(this.config.jumperWidth/3,this.config.jumperDeep/2,this.config.jumperHeight, 40)
    //球体 jumper的头
    var headGeometry = new THREE.SphereGeometry( this.config.jumperDeep/2, 32, 32 );
    bodyGeometry.translate(0 , 1, 0 )
    headGeometry.translate(0, 2.4, 0);
    this.jumperBody = new THREE.Mesh(bodyGeometry, material);   //网格模型 形状和材质
    this.jumperHead = new THREE.Mesh(headGeometry, material);
    this.jumperBody.castShadow = true; // 产生阴影
    this.jumperHead.castShadow = true; // 产生阴影

    var mesh = new THREE.Group(); //组合对象，把多个组合在一起。操作一组对象
    mesh.add(this.jumperBody);
    mesh.add(this.jumperHead);
    mesh.position.y = 3;
    mesh.position.x = this.config.jumperWidth / 2;
    mesh.position.z = this.config.jumperWidth / 2;

    this.jumper = mesh
    this.scene.add(this.jumper);

    this.directionalLight.target = this.jumper; // 将平行光跟随jumper

    function jumperInitFall() {
      if (self.jumper.position.y > 1) { //在方块上方
        self.jumper.position.y -= 0.1
        self._render(self.scene, self.camera)
        requestAnimationFrame(function () {
          jumperInitFall();
        })
      }
    }
    jumperInitFall();

  },
  // 新增一个方块, 新的方块有2个随机方向
  _createCube: function () {
    var geometryObj = this._createGeometry(); // 生成一个集合体
    var materialObj = this._createMaterial()(); // 生成材质

    var mesh = new THREE.Mesh(geometryObj.geometry, materialObj.material)
    mesh.castShadow = true; // 产生阴影
    mesh.receiveShadow = true;    // 接收阴影

    if( this.cubes.length ) {
      //0-0.5  方块随机在左  反之在右
      this.cubeStat.nextDir =  Math.random() > 0.5 ? 'left' : 'right'
      mesh.position.x = this.cubes[this.cubes.length - 1].position.x
      mesh.position.y = this.cubes[this.cubes.length - 1].position.y
      mesh.position.z = this.cubes[this.cubes.length - 1].position.z
      if (this.cubeStat.nextDir === 'left') { 
        mesh.position.x = this.cubes[this.cubes.length - 1].position.x-4*Math.random() - 6
      } else {
        mesh.position.z = this.cubes[this.cubes.length - 1].position.z-4*Math.random() - 6
      }
    }

    this.cubes.push(mesh)
    // 当方块数大于6时，删除前面的方块，因为不会出现在画布中
    if (this.cubes.length > 6) {
      this.scene.remove(this.cubes.shift())
    }
    this.scene.add(mesh)
    // 每新增一个方块，重新计算摄像机坐标
    if ( this.cubes.length > 1) {
      this._updateCameraPos();
    }

  },
  _render: function () {  //渲染
    this.renderer.render(this.scene, this.camera)
  },
  _setLight: function () {
    var light = new THREE.AmbientLight( 0xffffff, 0.3 )   //环境光
    this.scene.add( light )

    this.directionalLight = new THREE.DirectionalLight( 0xffffff , 10);   //平行光
    this.directionalLight.distance = 0;
    this.directionalLight.position.set( 60, 50, 40 )  //灯光的位置属性
    this.directionalLight.castShadow = true; // 产生阴影
    this.directionalLight.intensity = 0.5;  //光线的密度
    this.scene.add(this.directionalLight)
  },
  _setCamera: function () { //设置相机和相机看向的点
    this.camera.position.set(100, 100, 100)
    this.camera.lookAt(this.cameraPos.current)
  },
  _setRenderer: function () { //渲染
    this.renderer.setSize(this.size.width, this.size.height)
    this.renderer.setClearColor(this.config.background)
    this.renderer.shadowMap.enabled = true; // 开启阴影

    document.body.appendChild(this.renderer.domElement)   //插入一个dom元素
  },
  _setSize: function () {   //返回窗口的文档显示区的宽度 高度
    this.size.width = window.innerWidth,
    this.size.height = window.innerHeight
  },
  _createMaterial: function(){ // 生成材质/贴图
    var config = this.config;
     // 所有的材质数组
    var materials = [
      {
        material : new THREE.MeshLambertMaterial({color: config.cubeColor}),
        type: 'DefaultCubeColor'
      },
      RandomColor(),    //随机颜色
      clockMaterial(),  //时钟方块
      RadialGradient(),   //径向渐变创建 "图像"
      RadialGradient2(),
      Chess(),    //棋盘格方块
      wireFrame(),
    ];

    return function (idx) {
      if(idx == undefined){ //undefined 属性用于存放 JavaScript 中未定义的值
        idx = Math.floor(Math.random()*materials.length);
      }
      return materials[idx];
    }

    function clockMaterial() {    //时钟方块
      var texture;
      var matArray = []; // 多贴图数组

      texture = new THREE.CanvasTexture(canvasTexture); // 此处的canvasTexture来自canvas.texture.js文件
      texture.needsUpdate = true;

      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));  //创建暗淡的并不光亮的表面
      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));
      matArray.push(new THREE.MeshBasicMaterial({ map: texture })); //这种材质不考虑场景中光照的影响。使用这种材质的网格会被渲染成简单的平面多边形
      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));
      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));
      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));

      return {
        material : matArray,
        type: 'Clock'
      }
    }

    function RadialGradient() {
      var texture;
      var matArray = []; // 多贴图数组

      var canvasTexture1 = document.createElement("canvas");  //纹理贴图
      canvasTexture1.width=16;
      canvasTexture1.height=16;
      var ctx= canvasTexture1.getContext("2d");
      // 创建渐变
      /* 参数	描述
        x0	渐变的开始圆的 x 坐标
        y0	渐变的开始圆的 y 坐标
        r0	开始圆的半径
        x1	渐变的结束圆的 x 坐标
        y1	渐变的结束圆的 y 坐标
        r1	结束圆的半径*/
      var grd=ctx.createRadialGradient(50,50,32,60,60,100);
      //渐变需要添加两种结束颜色
      grd.addColorStop(0,"red");
      grd.addColorStop(1,"white");
      // 填充渐变
      ctx.fillStyle=grd;
      ctx.fillRect(10,10,150,80);

      texture = new THREE.CanvasTexture(canvasTexture1);  //纹理贴图
      texture.needsUpdate = true;
      texture.wrapS = texture.wrapT = THREE.RepeatWrapping; // 指定重复方向为两个方向
      texture.repeat.set(5,5); // 设置重复次数都为4

      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));
      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));
      matArray.push(new THREE.MeshBasicMaterial({ map: texture }));
      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));
      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));
      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));

      return {
        material : matArray,
        type: 'RadialGradient'
      }
    }

    function RadialGradient2() {

      var canvas = document.createElement('canvas');
      canvas.width = 16;
      canvas.height = 16;

      var context = canvas.getContext('2d');
      var gradient = context.createRadialGradient(canvas.width / 2, canvas.height / 2, 0, canvas.width / 2, canvas.height / 2, canvas.width / 2);
      gradient.addColorStop(0, 'rgba(255,255,255,1)');
      gradient.addColorStop(0.2, 'rgba(0,255,255,1)');
      gradient.addColorStop(0.4, 'rgba(0,0,64,1)');
      gradient.addColorStop(1, 'rgba(0,0,0,1)');

      context.fillStyle = gradient;
      context.fillRect(0, 0, canvas.width, canvas.height);

      var matArray = []; // 多贴图数组
      var texture = new THREE.Texture(canvas);
      texture.needsUpdate = true;

      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));
      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));
      matArray.push(new THREE.MeshBasicMaterial({ map: texture }));
      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));
      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));
      matArray.push(new THREE.MeshLambertMaterial({color: config.cubeColor}));

      return {
        material : matArray,
        type: 'RadialGradient2'
      }
    }

    function Chess() {    //棋盘方块
      var texture = new THREE.TextureLoader().load('https://raw.githubusercontent.com/Ovilia/ThreeExample.js/master/img/chess.png');
      texture.wrapS = texture.wrapT = THREE.RepeatWrapping; // 指定重复方向为两个方向
      texture.repeat.set(2, 2); // 设置重复次数都为4

      return {
        material : new THREE.MeshBasicMaterial( { map: texture } ),
        type : 'Chess'
      }
    }

    function RandomColor(){
      var color = '#'+Math.floor(Math.random()*16777215).toString(16);
      return {
        material : new THREE.MeshLambertMaterial({color: color}),
        type : 'RandomColor',
        color : color,
      }
    }

    function wireFrame(){ //线性框
      return {
        material: new THREE.MeshLambertMaterial({color: config.cubeColor, wireframe: true}),
        type: 'wireFrame'
      }
    }

  },
  _createGeometry: function(){ // 生成几合体
    var obj = {};
    if(Math.random() > 0.5){  // 添加圆柱型方块
      obj.geometry = new THREE.CylinderGeometry(this.config.cubeWidth / 2, this.config.cubeWidth / 2, this.config.cubeHeight, 40)
      obj.type = 'CylinderGeometry';
    }else{ // 方块
      obj.geometry = new THREE.CubeGeometry(this.config.cubeWidth,this.config.cubeHeight,this.config.cubeDeep)
      obj.type = 'CubeGeometry';
    }
    return obj;
  }

}